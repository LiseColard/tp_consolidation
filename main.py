## File main 

from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}




### Exploration de l'API
#Code sur le TP
requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
print(requete.status_code)
print(requete.json)
#Fonction: 
def fonction_get():
    sortie = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
    return(sortie.json)

### Tests unitaire sur les fonctions
def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
          if ingredient["vegan"]=='no' or ingredient["vegan"]=='maybe':
            return False
    return True


### Importation des données 
import json
def load_params_from_json(json_path):
    with open(json_path) as f:
        return json.load(f)
